package com.aswdc.myapp;

import android.os.Bundle;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;


import com.aswdc.myapp.adapter.Userlistadapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class SecondActivity extends AppCompatActivity {

    ListView lvDisplay;
    ArrayList<HashMap<String, Object>> takenUserList = new ArrayList<>();
    Userlistadapter userListAdapeter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);

        initViewRefernce();
        bindData();
    }

    void bindData() {
        takenUserList.addAll((Collection<? extends HashMap<String, Object>>) getIntent().getSerializableExtra("UserList"));
        userListAdapeter = new Userlistadapter(this, takenUserList);
        lvDisplay.setAdapter(userListAdapeter);

    }

    void initViewRefernce() {
        lvDisplay = findViewById(R.id.lvActDisplay);
    }
}
