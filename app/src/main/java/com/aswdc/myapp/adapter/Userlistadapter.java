package com.aswdc.myapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aswdc.myapp.R;
import com.aswdc.myapp.util.Constant;

import java.util.ArrayList;
import java.util.HashMap;

import static java.lang.String.valueOf;

public class Userlistadapter extends BaseAdapter {
    Context context;
    ArrayList<HashMap<String, Object>> userlist;

    public Userlistadapter(Context context, ArrayList<HashMap<String, Object>> userlist) {
        this.context = context;
        this.userlist = userlist;
    }

    @Override
    public int getCount() {
        return userlist.size();
    }

    @Override
    public Object getItem(int i) {
        return userlist.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View view1 = LayoutInflater.from(context).inflate(R.layout.list_view, null);

        TextView tvname = view1.findViewById(R.id.tvActName);
        TextView tvemail = view1.findViewById(R.id.tvActEmail);
        TextView tvmale = view1.findViewById(R.id.tvActMale);
        TextView tvfemale = view1.findViewById(R.id.tvActFemale);

        tvname.setText(valueOf(userlist.get(position).get(Constant.FIRST_NAME)));
        tvemail.setText(valueOf(userlist.get(position).get(Constant.EMAIL_ADDRESS)));

        if (valueOf(userlist.get(position).get(Constant.GENDER)).equals("Male")) {
            tvfemale.setVisibility(View.GONE);
        } else {
            tvmale.setVisibility(View.GONE);
        }

        return view1;
    }
}
